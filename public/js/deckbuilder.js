var app = angular.module('deckbuilderApp', ['googlechart']);

app.config(['$compileProvider',
	function ($compileProvider) {
		$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);
}]);

app.directive('imageonload', function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			// console.log("in directive");
			element.bind('load', function() {
				scope.$apply(attrs.imageonload);
				console.log("done loading image");
			});
			element.bind('error', function() {
				console.log('image could not be loaded');
			});
		}
	};
});

app.controller('deckbuilderCtrl', function($scope, $http, $filter) {
	
	$scope.updateDeckInfo = function(){
		console.log("Deck info updated");
		$scope.deck.name = $scope.fileName;
		$scope.deck.format = $scope.format.selectedFormat.name;
		$scope.calculateDevotion();
		$scope.calculateManaCurve();
		$scope.calculateTypes();
		var content = JSON.stringify($scope.deck);
		var blob = new Blob([ content ], { type : 'text/plain' });
		$scope.url = (window.URL || window.webkitURL).createObjectURL( blob );
	}
	
	//Start deckbuiling code
	$scope.setsAreLoading = true;
	$http.get('https://api.deckbrew.com/mtg/sets')
		.then(function(response) {
			$scope.sets = response.data;
			$scope.setsAreLoading = false;
		});

	$scope.setSelected = function() {
		$scope.cardsAreLoading = true;
		$scope.showCardImg = false;
		$scope.cards = [];
		getCards($http, $scope, 'https://api.deckbrew.com/mtg/cards?set=' + $scope.selectedSet.id);
	}

	$scope.cardSelected = function() {
		$scope.showCardImg = true;
		console.log("opacity down");
		$scope.cardImgStyle = "opacity: 0.4;";
		var newUrl = $filter("filter")
			($scope.selectedCard.editions, {
				set_id: $scope.selectedSet.id
			})[0].image_url;
		if ($scope.cardImgUrl == newUrl) {
			$scope.cardImgStyle = "opacity: 1.0;";
		}
		$scope.cardImgUrl = newUrl;
		console.log("img URL updated to ",$scope.cardImgUrl);
	}

	$scope.imageLoaded = function() {
		$scope.cardImgStyle = "opacity: 1.0;";
		console.log("opacity up");
	}

	$scope.format = {
		availableFormats:[
			{id: '1', name: 'Modern'},
			{id: '2', name: 'Block'},
			{id: '3', name: 'Standard'},
			{id: '4', name: 'Commander'}
		],
		selectedFormat: {id: '1', name: 'Modern'}
	};

	$scope.saveToPc = function(){
		$scope.deck.name = $scope.fileName;
		$scope.deck.format = $scope.format.selectedFormat.name;
		var json = JSON.stringify($scope.deck);
		var blob = new Blob([json], {type: "application/json"});
		var url = URL.createObjectURL(blob);
		var a = document.createElement('a');
		a.download = $scope.fileName;
		a.href = url;
		a.textContent = "Download " + $scope.fileName;
		document.getElementById('myFile').appendChild(a);
	};

	$scope.deck = { 
		"name": "Deck in Progress",
		"format": "Select Format Below",
		"colors": [],
		"deck": []
	};
	$scope.deck_part = 'main';

	$scope.display = [];

	$scope.addToDeck = function() {
		var single_object = $filter('filter')($scope.selectedCard.editions, function (d) {return d.set_id === $scope.selectedSet.id;})[0];
		if($scope.selectedCard.colors){
			var newColor = $scope.deck.colors.indexOf($scope.selectedCard.colors[0]);
			if(newColor === -1){
				$scope.deck.colors.push($scope.selectedCard.colors[0]);
			}
		}
		$scope.deck.deck.push({multiverse_id:single_object.multiverse_id,qty:$scope.qty,deck_part:$scope.deck_part});
		$scope.display.push({name:$scope.selectedCard.name,quantity:$scope.qty,image:$scope.cardImgUrl,deck_part:$scope.deck_part});
		
		$scope.updateDeckInfo();
	};

	$scope.toggleExport = function() {
		$scope.showExport = !$scope.showExport;
	};
	
	//Start Charting
	var chartTypes = {};
	chartTypes.type = "PieChart";
	chartTypes.data = [
		['Type', 'qty']
	];
	
	chartTypes.data.push(['Artifact', 0]);
	chartTypes.data.push(['Creature', 0]);
	chartTypes.data.push(['Enchantment', 0]);
	chartTypes.data.push(['Instant', 0]);
	chartTypes.data.push(['Land', 0]);
	chartTypes.data.push(['Planeswalker', 0]);
	chartTypes.data.push(['Tribal', 0]);
	chartTypes.data.push(['Sorcery', 0]);
	
	chartTypes.options = {
		displayExactValues: true,
		pieSliceTextStyle: {
			color: 'black',
		},
		width: 400,
		height: 200,
		is3D: false,
		chartArea: {
			left: 10,
			top: 10,
			bottom: 0,
			height: "100%"
		}
	};
	
	var chartDevotion = {};
	chartDevotion.type = "PieChart";
	chartDevotion.data = [
		['Color', 'cost']
	];

	chartDevotion.data.push(['White', 0]);
	chartDevotion.data.push(['Blue', 0]);
	chartDevotion.data.push(['Black', 0]);
	chartDevotion.data.push(['Red', 0]);
	chartDevotion.data.push(['Green', 0]);
	chartDevotion.data.push(['Colorless', 0]);

	chartDevotion.options = {
		displayExactValues: true,
		pieSliceTextStyle: {
			color: 'black',
		},
		width: 400,
		height: 200,
		is3D: false,
		chartArea: {
			left: 10,
			top: 10,
			bottom: 0,
			height: "100%"
		},
		colors: ['#faf4e9', '#327bde', '#7e7a7a', '#dc3912', '#34ad40', '#c7c7c7']
	};

	chartDevotion.formatters = {
		number: [{
			columnNum: 1,
			pattern: "#,##0"
		}]
	};

	$scope.devChart = chartDevotion;
	$scope.typeChart = chartTypes;

	$scope.manaCurve = [];

	$scope.showExport = false;
	$scope.devotionCalculated = false;
	$scope.typesCalculated = false;

	$scope.calculateManaCurve = function() {
		$scope.manaCurve = [];
		for (var i in $scope.deck.deck) {
			var card = $scope.deck.deck[i];
			$scope.addToManaCurve(card);
		}
	};

	$scope.calculateDevotion = function() {
		for(var i = 1; i <= 6; i++){
			$scope.devChart.data[i][1] = 0;
		}
		for (var i in $scope.deck.deck) {
			var card = $scope.deck.deck[i];
			$scope.addToDevotion(card);
		}
		$scope.devotionCalculated = true;
	};

	$scope.calculateTypes = function() {
		for(var i = 1; i <= 8; i++){
			$scope.typeChart.data[i][1] = 0;
		}
		for (var i in $scope.deck.deck) {
			var card = $scope.deck.deck[i];
			$scope.addToTypes(card);
		}
		$scope.typesCalculated = true;
	};

	$scope.addToManaCurve = function(card) {
		var qty = card.qty;
		var deck_part = card.deck_part;

		$http.get('https://api.deckbrew.com/mtg/cards?multiverseid=' + card.multiverse_id).success((function(qty, deck_part) {
			return function(data) {
				if (data[0].types[0] == "land" || deck_part != "main") {
					console.log("It's a land or it isn't in the main deck; we are not going to add it to the calculation " + data[0].name);
				} else {
					if ($scope.manaCurve[data[0].cmc] === undefined) {
						$scope.manaCurve[data[0].cmc] = 0;
					}
					$scope.manaCurve[data[0].cmc] += qty;
					console.log("There are " + $scope.manaCurve[data[0].cmc] + " cards with CMC of " + data[0].cmc + " and the qty is " + qty + " " + data[0].name);
				}
			}
		})(qty, deck_part));
	};

	$scope.addToTypes = function(card) {
		var qty = card.qty;
		var deck_part = card.deck_part;

		$http.get('https://api.deckbrew.com/mtg/cards?multiverseid=' + card.multiverse_id).success((function(qty, deck_part) {
			return function(data) {
				if (deck_part == "main") {
					var types = data[0].types;
					for (var index in types) {
						switch (types[index]) {
							case "artifact":
								$scope.typeChart.data[1][1] += 1 * qty;
								break;
							case "creature":
								$scope.typeChart.data[2][1] += 1 * qty;
								break;
							case "enchantment":
								$scope.typeChart.data[3][1] += 1 * qty;
								break;
							case "instant":
								$scope.typeChart.data[4][1] += 1 * qty;
								break;
							case "land":
								$scope.typeChart.data[5][1] += 1 * qty;
								break;
							case "planeswalker":
								$scope.typeChart.data[6][1] += 1 * qty;
								break;
							case "tribal":
								$scope.typeChart.data[7][1] += 1 * qty;
								break;
							case "sorcery":
								$scope.typeChart.data[8][1] += 1 * qty;
								break;
						}
					}
				}
			}
		})(qty, deck_part));
	};

	$scope.addToDevotion = function(card) {
		var qty = card.qty;
		var deck_part = card.deck_part;

		$http.get('https://api.deckbrew.com/mtg/cards?multiverseid=' + card.multiverse_id).success((function(qty, deck_part) {
			return function(data) {
				if (deck_part == "main") {
					var colors = data[0].colors;
					// for (var i in colors) {
						// console.log(colors[i]);
					// }
					// console.log(data[0].cost);
					var cost = data[0].cost;
					for (i = 0; i < cost.length; i++) {
						switch (cost[i]) {
							case "W":
								$scope.devChart.data[1][1] += 1 * qty;
								break;
							case "U":
								$scope.devChart.data[2][1] += 1 * qty;
								break;
							case "B":
								$scope.devChart.data[3][1] += 1 * qty;
								break;
							case "R":
								$scope.devChart.data[4][1] += 1 * qty;
								break;
							case "G":
								$scope.devChart.data[5][1] += 1 * qty;
								break;
							case "C":
								$scope.devChart.data[6][1] += qty;
						}
					}
				}
			};
		})(qty, deck_part));
	};
});




function getCards($http, $scope, url) {
	// console.log("Getting", url);
	$http.get(url)
		.then(function(response) {
			if (response.data.length > 0) {
				next_page = parse_link(response.headers().link).next;
				response.data.forEach(function(c) {
					$scope.cards.push(c);
				});
				getCards($http, $scope, next_page);
			} else {
				$scope.cardsAreLoading = false;
			}
		});
}

function parse_link(header) {
	if (header.length == 0) {
		console.log("input must not be of zero length");
	}
	var parts = header.split(',');
	var links = {};
	parts.forEach(function(p) {
		var section = p.split(';');
		if (section.length != 2) {
			console.log("section could not be split on ';'");
		}
		var url = section[0].replace(/<(.*)>/, '$1').trim();
		var name = section[1].replace(/rel="(.*)"/, '$1').trim();
		links[name] = url;
	});
	return links;
}
