var app = angular.module('listSets', []);

app.directive('imageonload', function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			console.log("in directive");
			element.bind('load', function() {
				scope.$apply(attrs.imageonload);
				console.log("done loading image");
			});
			element.bind('error', function() {
				console.log('image could not be loaded');
			});
		}
	};
});

app.controller('listSetsCtrl', function($scope, $http, $filter) {
	$scope.setsAreLoading = true;
	$http.get('https://api.deckbrew.com/mtg/sets')
		.then(function(response) {
			$scope.sets = response.data;
			$scope.setsAreLoading = false;
		});

	$scope.setSelected = function() {
		$scope.cardsAreLoading = true;
		$scope.showCardImg = false;
		$scope.cards = [];
		getCards($http, $scope, 'https://api.deckbrew.com/mtg/cards?set=' + $scope.selectedSet.id);
	}

	$scope.cardSelected = function() {
		$scope.showCardImg = true;
		console.log("opacity down");
		$scope.cardImgStyle = "opacity: 0.4;";
		var newUrl = $filter("filter")
			($scope.selectedCard.editions, {
				set_id: $scope.selectedSet.id
			})[0].image_url;
		if ($scope.cardImgUrl == newUrl) {
			$scope.cardImgStyle = "opacity: 1.0;";
		}
		$scope.cardImgUrl = newUrl;
		console.log("img URL updated to ",$scope.cardImgUrl);
	}

	$scope.imageLoaded = function() {
		$scope.cardImgStyle = "opacity: 1.0;";
		console.log("opacity up");
	}

	$scope.format = { availableFormats:[
	{id: '1', name: 'Modern'},
	{id: '2', name: 'Block'},
	{id: '3', name: 'Standard'},
	{id: '4', name: 'Commander'}],
	selectedFormat: {id: '1', name: 'Modern'}
	};

        $scope.saveToPc = function () {
	$scope.deck.name = $scope.fileName;
	$scope.deck.format = $scope.format.selectedFormat.name;
        var json = JSON.stringify($scope.deck);
        var blob = new Blob([json], {type: "application/json"});
        var url = URL.createObjectURL(blob);
        var a = document.createElement('a');
        a.download = $scope.fileName;
        a.href = url;
        a.textContent = "Download " + $scope.fileName;
        document.getElementById('myFile').appendChild(a);
        };

        $scope.deck = { 
		"name": "Deck in Progress",
		"format": "Select Format Below",
		"colors": [],
		"deck": []};
        $scope.deck_part = 'main';
        $scope.addToDeck = function() {
        	var single_object = $filter('filter')($scope.selectedCard.editions, function (d) {return d.set_id === $scope.selectedSet.id;})[0];
		if($scope.selectedCard.colors){
		var newColor = $scope.deck.colors.indexOf($scope.selectedCard.colors[0]);
		if(newColor === -1){
		$scope.deck.colors.push($scope.selectedCard.colors[0]);}}
        	$scope.deck.deck.push({multiverse_id:single_object.multiverse_id,quantity:$scope.qty,deck_part:$scope.deck_part});
    	};

});

function getCards($http, $scope, url) {
	console.log("Getting", url);
	$http.get(url)
		.then(function(response) {
			if (response.data.length > 0) {
				next_page = parse_link(response.headers().link).next;
				response.data.forEach(function(c) {
					$scope.cards.push(c);
				});
				getCards($http, $scope, next_page);
			} else {
				$scope.cardsAreLoading = false;
			}
		});
}

function parse_link(header) {
	if (header.length == 0) {
		console.log("input must not be of zero length");
	}
	var parts = header.split(',');
	var links = {};
	parts.forEach(function(p) {
		var section = p.split(';');
		if (section.length != 2) {
			console.log("section could not be split on ';'");
		}
		var url = section[0].replace(/<(.*)>/, '$1').trim();
		var name = section[1].replace(/rel="(.*)"/, '$1').trim();
		links[name] = url;
	});
	return links;
}
