# MTG Sim
## A simple MTG deck builder and card play simulator
---
### Get Started
- Clone the repository
	- `git clone https://dpears@bitbucket.org/dpears/mtg-sim.git`
- Install node modules
	- `cd mtg-sim/ && npm install`
- Run the app
	- `npm start`
	- The app is now running on localhost:3000
